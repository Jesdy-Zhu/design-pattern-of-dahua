﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandPattern
{
    class Worker
    {
        public void Kebab()
        {
            Console.WriteLine("烤肉串");
        }

        public void BraisedChickenWings()
        {
            Console.WriteLine("烤鸡翅");
        }
    }
}
