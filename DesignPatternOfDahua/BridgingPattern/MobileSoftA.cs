﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgingPattern
{
    class MobileSoftA : MobileSoft
    {
        public override void Run()
        {
            Console.WriteLine("实现软件A的功能");
        }
    }
}