﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BridgingPattern
{
    abstract class MobileSoft
    {
        public abstract void Run();
    }
}
